
$( document ).ready(function() {
   	

	winHeight = $( window ).height();
	$( "#cover" ).height(winHeight);

	$("#puzzles").wrapInner('<div class="page-content"></div>')
	$("#pre-test").wrapInner('<div class="page-content"></div>')
	$("#chapter-summary").wrapInner('<div class="page-content"></div>')
	$("#semester-review").wrapInner('<div class="page-content"></div>')
	$(".key-ideas").wrapInner('<div class="content"></div>')

	// Adds a entity to render the margin col without any content
	$(".margin.hidden").html('&nbsp;')



	$( window ).resize(function() {
		winHeight = $( window ).height();
	  	$( "#cover" ).height(winHeight);
	});


	// Head tip config
   	Tipped.create('.headtip',{
		position:'topleft',
		maxWidth: 320,
		size: 'large',
	});

});



